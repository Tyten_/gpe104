﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{

    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        void Awake()
        {
           
            if (instance == null)
            {
                instance = this; 
                DontDestroyOnLoad(gameObject); 
            }
            else
            {
                Destroy(this.gameObject);
                Debug.Log("Warning: A second game manager was detected and destroyed.");
            }
        }
    }
}