﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heatseeker : MonoBehaviour {

	public Transform targetTf;
	private Transform tf;
	public float speed;
	public bool isAlwaysSeeking;
	private Vector3 movementVector;
	public bool isDirectional;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		// Target the player
		movementVector = targetTf.position - tf.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (isAlwaysSeeking) {
			movementVector = targetTf.position - tf.position; // end position - start position
		}

		// Move Every Framedraw
		movementVector.Normalize(); // make it a length of 1
		movementVector = movementVector * speed; // make it a length of 'speed'
		tf.position = tf.position + movementVector; // move down that vector

		// If we need to, look at the player
		if (isDirectional) {
			tf.up = movementVector;
		}
	}
}
