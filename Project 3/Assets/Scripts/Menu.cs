﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Menu : MonoBehaviour {

    public Button playButton;
    public Button exitButton;


	// Use this for initialization
	void Start () {
        playButton.onClick.AddListener(() => { SceneManager.LoadScene("Game"); });
        exitButton.onClick.AddListener(() => { Application.Quit(); });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
