﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMover : MonoBehaviour {

   public SpriteRenderer sr;// define the sprite renderer   
    public Transform tf;// define the transformation 
    

	// Use this for initialization
	void Start () {

        sr = GetComponent<SpriteRenderer>();//get the sprite renderer
        tf = GetComponent<Transform>();//get the transform library 
        tf.position = Vector3.zero;// set poistion of player to 0,0,0
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.LeftArrow))//when you hit left
        {
            transform.position = tf.position + (Vector3.left * 0.3f);// move left
        }
        if (Input.GetKey(KeyCode.RightArrow))//when you hit right
        {
            transform.position = tf.position + (Vector3.right * 0.3f);// move right
        }
      
        if (Input.GetKeyDown(KeyCode.Space))// when you hit space
        { 
            transform.position = Vector3.zero;// reset position
            Debug.Log("Sprite Reset");//in the log display sprite has been reset
        }
        if (Input.GetKey(KeyCode.Alpha1))
        {
            sr.color = Color.blue;
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            sr.color = Color.green;
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            sr.color = Color.red;
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            sr.color = Color.yellow;
        }
        if (Input.GetKey(KeyCode.Alpha5))
        {
            sr.color = Color.black;
        }
        if (Input.GetKey(KeyCode.Alpha6))
        {
            sr.color = Color.cyan;
        }
        if (Input.GetKey(KeyCode.Alpha7))
        {
            sr.color = Color.gray;
        }
        if (Input.GetKey(KeyCode.Alpha8))
        {
            sr.color = Color.white;
        }
        if (Input.GetKey(KeyCode.Alpha9))
        {
            sr.color = Color.magenta;
        }
        if (Input.GetKey(KeyCode.Alpha0))
        {
            sr.color = Color.clear;
        }
    }   
        
      

}
