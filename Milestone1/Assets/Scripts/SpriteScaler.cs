﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScaler : MonoBehaviour {
    private Transform tf;// define transform function as private
    public float maxScale;// define maxscale as a float value
	// Use this for initialization
	void Start () {
        
        tf = GetComponent<Transform>();	
	}
	
	// Update is called once per frame
	void Update () {
        float axesValue = Input.GetAxis("Scale");// get the input information from scale and when it is pressed

        float scaleAmount = axesValue * maxScale;// scale the size of the sprite 
        tf.localScale += Vector3.one * scaleAmount; 
	}
}
