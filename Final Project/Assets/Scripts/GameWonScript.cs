﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameWonScript : MonoBehaviour {
    public Button playButton;
    public Button exitButton;
    public Button retryButton;


    // Use this for initialization
    void Start()
    {
        retryButton.onClick.AddListener(() => { SceneManager.LoadScene("Level 1"); });
        playButton.onClick.AddListener(() => { SceneManager.LoadScene("Main Menu"); });
        exitButton.onClick.AddListener(() => { Application.Quit(); });
    }


    // Update is called once per frame
    void Update () {
		
	}
}
