﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotPawn : Pawn
{

    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Transform tf;
    private Animator anim;

    public float moveSpeed;
    public float jumpForce;
    public bool isGrounded;
    public float groundDistance = 0.1f;

    public void Start()
    {
        // Get my components
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        tf = GetComponent<Transform>();
        anim = GetComponent<Animator>();
    }

    public void Update()
    {
        // Check for Grounded
        CheckForGrounded();
    }

    public void CheckForGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector3.down, groundDistance);
        if (hitInfo.collider == null)
        {
            isGrounded = false;
        }
        else
        {
            isGrounded = true;
            Debug.Log("Standing on " + hitInfo.collider.name);
        }
    }

    public override void Move(Vector2 moveVector)
    {
        // Change X velocity based on speed and moveVector
        rb.velocity = new Vector2(moveVector.x * moveSpeed, rb.velocity.y);

        // If velocity is <0, flip sprite. 
        if (rb.velocity.x < 0)
        {
            anim.Play("Walking");
            sr.flipX = true;
        }
        else if (rb.velocity.x > 0)
        {
            anim.Play("Walking");
            sr.flipX = false;
        }
        else
        {
            anim.Play("Idle");
        }
    }

    public override void Jump()
    {
        if (isGrounded)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
        }
    }

}
