﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : Controller {

    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode rightKey = KeyCode.RightArrow;
    public KeyCode leftKey = KeyCode.LeftArrow;
    public KeyCode upKey = KeyCode.UpArrow;
    public KeyCode downKey = KeyCode.DownArrow;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        // Aggregate movement each frame, and always move
        Vector2 movementVector = Vector2.zero;
        if (Input.GetKey(leftKey))
        {
            movementVector.x = -1;
        }
        if (Input.GetKey(rightKey))
        {
            movementVector.x = 1;
        }
        if (Input.GetKey(upKey))
        {
            movementVector.y = 1;
        }
        if (Input.GetKey(downKey))
        {
            movementVector.y = -1;
        }

        pawn.Move(movementVector);

        // Jump
        if (Input.GetKeyDown(jumpKey))
        {
            pawn.Jump();
        }

    }
}