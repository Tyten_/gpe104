﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	// Use this for initialization
	void Start () {
		// Set up singleton
		if (instance == null) {
			instance = this;
		}
        else
        {
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
