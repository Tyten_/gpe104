﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

    public virtual void Move(Vector2 moveVector)
    {
    }

    public virtual void Jump()
    {
    }

}
