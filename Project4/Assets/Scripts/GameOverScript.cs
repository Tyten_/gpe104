﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameOverScript : MonoBehaviour {
    
    public Button retryButton;
    public Button exitButton;
        // Use this for initialization
        void Start () {
        retryButton.onClick.AddListener(() => { SceneManager.LoadScene("Platformer"); });
        exitButton.onClick.AddListener(() => { Application.Quit(); });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
